<?php

return [
    // This is role will be auto assign to user when user logged using OAuth ex: goggle, facebook
    'id_role_default' => 3,

    'id_role_type' => [
        'super_admin' => 1
    ],

    /**
     * List module will be append into application
     */
    'modules' => [
        'User' => [
            'access'  => 'admin',
            'actions' => 'view,add,edit,delete'
        ],
        'Category' => [
            'access'  => 'admin',
            'actions' => 'view,add,edit,delete'
        ],
        'Blog'     => [
            'access'  => 'admin',
            'actions' => 'view,add,edit,delete'
        ],
        // => slug: view_category_group
    ]
];
