<?php

use App\Models\Role;
use App\Http\Resources\RoleResource;
use Illuminate\Support\Facades\Route;

include('backend/auth.php');
include('backend/public.php');
include('backend/test-api.php');

Route::group(['middleware' => 'auth:api'], function() {

    Route::get('module/get-all', 'ModuleController@getAll')->name('module.getAll');
    Route::get('role/get-all', function(){
        return RoleResource::collection(Role::all());
    })->name('role.getAll');
    //Admin routes
    Route::group(['prefix' => 'admin', 'as.' => 'admin'], function() {
        Route::apiResource('role', 'RoleController');
        Route::apiResource('user', 'UserController');
        Route::apiResource('blog', 'BlogController');
    });
});
// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
