<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'auth',
    'as' => 'auth.'
], function() {
    Route::post('login', 'Auth\AuthController@login')->name('login');
    Route::post('forgot-password', 'Auth\AuthController@forgotPassword')->name('forgotPassword');
    Route::patch('reset-password', 'Auth\AuthController@resetPassword')->name('resetPassword');
    Route::post('logout', 'Auth\AuthController@logout')->name('logout')->middleware('auth:api');
    Route::get('user', 'Auth\AuthController@user')->name('user')->middleware('auth:api');
});
