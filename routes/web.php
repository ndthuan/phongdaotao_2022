<?php

use Illuminate\Support\Facades\Route;

Route::get('/{path}', 'Api\ApiController')->where('path', '(.*)');
