<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('blogs')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $categories = DB::table('categories')->pluck('id')->toArray();

        DB::table('blogs')->insert([
            [
                'title'       => 'Blog 1',
                'slug'        => 'blog-1',
                'content'     => '</h1>Content blog 1<h1>',
                'category_id' => $categories[array_rand($categories)],
                'created_at'  => now(),
                'updated_at'  => now(),
            ],
            [
                'title'       => 'Blog 2',
                'slug'        => 'blog-2',
                'content'     => '</h1>Content blog 2<h1>',
                'category_id' => $categories[array_rand($categories)],
                'created_at'  => now(),
                'updated_at'  => now(),
            ],
            [
                'title'       => 'Blog 3',
                'slug'        => 'blog-3',
                'content'     => '</h1>Content blog 3<h1>',
                'category_id' => $categories[array_rand($categories)],
                'created_at'  => now(),
                'updated_at'  => now(),
            ],
        ]);
    }
}
