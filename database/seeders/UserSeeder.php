<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    const PASSWORD = 'Blog@12345';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('users')->insert([
            [
                'name'     => 'Super Admin',
                'email'    => 'phamdinhhung28.it@gmail.com',
                'role_id'  => 1,
                'password' => Hash::make(self::PASSWORD)
            ],
            [
                'name'     => 'User test 1',
                'email'    => 'usertest1@gmail.com',
                'role_id'  => 2,
                'password' => Hash::make(self::PASSWORD)
            ],
            [
                'name'     => 'User test 2',
                'email'    => 'usertest2@gmail.com',
                'role_id'  => 2,
                'password' => Hash::make(self::PASSWORD)
            ]
        ]);
    }
}
