<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('tags')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        DB::table('tags')->insert([
            [
                'title' => 'Thông báo',
                'slug'  => 'thong-bao',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'title' => 'Tốt nghiệp',
                'slug'  => 'tot-nghiep',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
