<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permissions')->truncate();
        \DB::table('permission_role')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $modules = DB::table('modules')->where('access', '!=', 'Super Admin')->get();

        foreach ($modules as $module) {
            $actions = explode(',', $module->actions);

            foreach ($actions as $action) {
                $slug = strtolower($action . '_' . Str::snake($module->name));

                if (! DB::table('permissions')->where('slug', $slug)->first()) {
                    DB::table('permissions')->insert([
                        'module_id' => $module->id,
                        'name'       => Str::title($action),
                        'slug'       => $slug,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
        }

        $permissionViews = \DB::table('permissions')->where('name', 'View')->pluck('id')->toArray();
        // dd($permissionViews);

        foreach ($permissionViews as $item) {
            \DB::table('permission_role')->insert([
                'permission_id' => $item,
                'role_id' => 2,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
