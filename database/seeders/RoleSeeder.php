<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        /**
         * Role 1: Super admin -> permissions: full
         * Role 2: New number -> permissions: full view
         */

        DB::table('roles')->insert([
            [
                'id'          => config('role.id_role_type.super_admin'),
                'name'        => 'Super Admin',
                'description' => 'Get full rights with no limits',
                'level'       => 1,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ],
            [
                'id'          => 2,
                'name'        => 'Role Demo',
                'description' => 'Get full rights view with no limits',
                'level'       => 2,
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ],
        ]);
    }
}
