<?php

namespace App\Services;

use Illuminate\Support\Arr;

class QueryService
{
    public $select = [];

    public $columnSearch = [];

    public $withRelationship = [];

    public $withOrWhere = [];

    public $search;

    public $ascending = 'desc';

    public $orderBy = 'created_at';

    public $scope = [];

    public $builder;

    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    public function queryTable()
    {
        $builder = $this->builder;

        $builder = $builder->when($this->select, function($q) {
            $q->select($this->select);
        });

        $builder = $builder->when($this->withOrWhere, function ($q) {
            foreach ($this->withOrWhere as $where) {
                $index = count($where) >= 3 ? 2: 1;
                if(!empty($where[$index])) {
                    $q->orWhere([$where]);
                }
            }
        });

        $builder = $builder->when($this->search, function ($q) {
            foreach (Arr::wrap($this->columnSearch) as $column) {
                $q->orWhere($column, 'LIKE', "%{$this->search}%");
            };
        });

        if(!empty($this->withRelationship)) {
            foreach (Arr::wrap($this->withRelationship) as $relationship) {
                $builder = $builder->with($relationship);
            }
        }

        if(!empty($this->scope)) {
            foreach(Arr::wrap($this->scope) as $scope) {
                $builder = $builder->$scope();
            }
        }

        $builder->orderBy($this->orderBy, $this->ascending);
        return $builder;

    }
}