<?php

namespace App\Http\Controllers\Api\v1\Auth;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Repositories\User\UserRepository;
use App\Notifications\Auth\SendVerificationEmail;
use App\Http\Requests\Validations\ResetPasswordRequest;

class AuthController extends Controller
{
    private $_userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->_userRepo = $userRepo;
    }

    public function login(Request $request)
    {
        try {
            $request->request->add([
                'username'      => $request->email,
                'password'      => $request->password,
                'grant_type'    => 'password',
                'client_id'     => config('services.passport.client_id'),
                'client_secret' => config('services.passport.client_secret'),
            ]);

            return $this->_getToken($request);
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }
    
    /**
     * Reset the given user's password.
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    public function forgotPassword(Request $request)
    {
        try {
            $email = $request->email;
            $user = User::where('email', $email)->first();

            if (! $user) {
                return $this->jsonError(trans('auth.password_reset_user'));
            }
            $passwordReset             = PasswordReset::firstOrNew(['email' => $email]);
            $passwordReset->email      = $email;
            $passwordReset->token      = Str::random(60);
            $passwordReset->created_at = Carbon::now();
            $passwordReset->save();
            $url = url("/reset-password/{$passwordReset->token}");
            $user->notify(new SendVerificationEmail($url, $passwordReset->token));

            return $this->jsonMessage('Send link to reset password successfully!');
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            $passwordResetBuilder = PasswordReset::where('token', $request->token);
            $passwordReset        = $passwordResetBuilder->first();
            if (
                !$passwordReset ||
                Carbon::parse($passwordReset->created_at)->addMinutes(PasswordReset::EXPIRE_TOKEN)->isPast()
            ) {
                return $this->jsonMessage(trans('validation.token_valid'), false);
            }
            $user           = $this->_userRepo->findBy('email', $passwordReset->email);
            $user->password = $request->password;
            $user->save();
            $passwordResetBuilder->delete();
            return $this->jsonMessage(trans('auth.reset_password_successfully'));
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    /**
     * Get token for user
     * @param Request $request
     * @return JsonResponse
     */
    private function _getToken($request)
    {
        $tokenRequest = $request->create(config('services.passport.login_endpoint'), 'POST');
        $response     = Route::dispatch($tokenRequest);

        switch($response->getStatusCode()) {
            case Response::HTTP_OK:
                return $this->jsonData(json_decode($response->getContent(), true));
            case Response::HTTP_BAD_REQUEST:
                return $this->jsonMessage(trans('auth.login_fail'), false, $response->getStatusCode());
            case Response::HTTP_UNAUTHORIZED:
                return $this->jsonMessage(trans('auth.credentials_incorrect'), false, $response->getStatusCode());
            default:
                return $this->jsonMessage($response->getContent(), false, $response->getStatusCode());
        }
    }

    public function logout(){
        try {
            Auth::user()->token()->delete();
            return $this->jsonMessage('Logged out!');
        } catch (\Throwable $e) {
            return $this->jsonError($e);

        }
    }

    public function user(){
        try {
            return $this->jsonData([
                'user'          => Auth::user(),
                'role'          => Auth::user()->role,
                'permissions'   => Auth::user()->role->permissions,
            ]);
        } catch (\Throwable $e) {
            return $this->jsonError($e);
        }
    }
}