<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ModuleRoleResource;

class ModuleController extends Controller
{
    public function getAll()
    {
        try {
            $modules = Module::get(['id', 'name', 'access', 'description']);

            return $this->jsonData(ModuleRoleResource::collection($modules));
        } catch (\Throwable $e) {
            return $this->jsonError($e);
        }
    }
}
