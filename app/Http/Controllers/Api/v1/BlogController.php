<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\BlogResource;
use App\Repositories\Blog\BlogRepository;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $_blogRepo;

    public function __construct(BlogRepository $blogRepo)
    {
        $this->_blogRepo = $blogRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $blogs = $this->_blogRepo->list($request);

            return $this->jsonTable([
                'data' => BlogResource::collection($blogs),
                'total' => $blogs->toArray()['total'],

            ]);

        } catch (\Throwable $th) {
            return $this->jsonError($th);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $blog = $this->_blogRepo->store($request);

            return $this->jsonData(new BlogResource($blog));
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $blog = $this->_blogRepo->find($id);

            if ($blog) {
                return $this->jsonData(new BlogResource($blog));
            }

            return $this->jsonMessage('Data not found!');
        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
