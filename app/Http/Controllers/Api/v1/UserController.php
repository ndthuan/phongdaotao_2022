<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use PhpParser\Node\Stmt\TryCatch;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repositories\User\UserRepository;
use App\Http\Requests\Validations\CreateUserRequest;
use App\Http\Requests\Validations\UpdateUserRequest;

class UserController extends Controller
{
    private $_userRepo;

    public function __construct(UserRepository $userRepository)
    {
        $this->_userRepo = $userRepository;
    }

    public function index(Request $request)
    {
        try {
            $users = $this->_userRepo->list($request);

            return $this->jsonTable([
                'data' => UserResource::collection($users),
                'total' => $users->toArray()['total'],

            ]);

        } catch (\Throwable $th) {
            return $this->jsonError($th);
        }
    }

    public function store(CreateUserRequest $request)
    {
        try {
            $user = $this->_userRepo->store($request);

            return $this->jsonData(new UserResource($user));
        } catch (\Throwable $th) {
            return $this->jsonError($th);
        }
    }

    public function show($id)
    {
        try {
            $user = $this->_userRepo->find($id);
            if($user) {
                return $this->jsonData(new UserResource($user));
            }

            return $this->jsonMessage('Data not found');
        } catch (\Throwable $th) {
            return $this->jsonError($th);
        }
    }

    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $user = $this->_userRepo->update($request, $id);
            return $this->jsonData(new UserResource($user));
        } catch (\Throwable $th) {
            return $this->jsonError($th);
        }
    }

    public function destroy($id) 
    {
        try {
            $this->_userRepo->destroy($id);
            return $this->jsonMessage('Deleted Successfully');
        } catch (\Throwable $th) {
            return $this->jsonError($th);
        }
    }
}
