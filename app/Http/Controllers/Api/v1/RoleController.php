<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Repositories\Role\RoleRepository;
use App\Http\Requests\Validations\CreateRoleRequest;
use App\Http\Requests\Validations\UpdateRoleRequest;

class RoleController extends Controller
{
    private $_roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->_roleRepository = $roleRepository;
    }

    public function index(Request $request)
    {
        try {
            $roles = $this->_roleRepository->list($request);
    
            return $this->jsonTable([
                'data'  => RoleResource::collection($roles),
                'total' => $roles->toArray()['total'],
            ]);

        } catch (\Exception $e) {
            return $this->jsonError($e);
        }
    }

    public function store(CreateRoleRequest $request)
    {
        try {
            $role = $this->_roleRepository->store($request);
            return $this->jsonData(new RoleResource($role));
        } catch (\Throwable $e) {
            return $this->jsonError($e);
        }
    }

    public function show($id) 
    {
        try {
            $role = $this->_roleRepository->find($id);
            if ($role) {
                return $this->jsonData(new RoleResource($role));
            }

            return $this->jsonMessage('Data not found!');
        } catch (\Throwable $e) {
            return $this->jsonError($e);
        }
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        try {
            $role = $this->_roleRepository->update($request, $id);
            return $this->jsonData(new RoleResource($role));
        } catch (\Throwable $e) {
            return $this->jsonError($e);
        }
    }

    public function destroy($id)
    {
        try {
            $this->_roleRepository->destroy($id);
            return $this->jsonMessage('Deleted Successfully!');
        } catch (\Throwable $e) {
            return $this->jsonError($e);
        }
    }

    public function getAll()
    {
        try {
            $roles = Role::get();

            return $this->jsonData(RoleResource::collection($roles));
        } catch (\Throwable $e) {
            return $this->jsonError($e);
        }
    }
}
