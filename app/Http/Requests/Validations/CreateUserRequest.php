<?php

namespace App\Http\Requests\Validations;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['name'] = 'bail|required';
        $rules['email'] = 'email|required|unique:users,email,'.Auth::user()->id;
        $rules['password'] = 'required|password_valid';

        return $rules;
    }
}
