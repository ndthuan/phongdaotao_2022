<?php

namespace App\Http\Requests\Validations;

use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class CreateRoleRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $rules['name'] = 'bail|required|unique:roles';

        if(Request::user()->accessLevel()){
            $rules['level'] = 'nullable|integer|between:'.Request::user()->accessLevel().', '.(99);
        }

        if (Request::input('level') && !Request::user()->accessLevel()) {
            Request::replace(['level' => null]);
        }
        return $rules;
    }
}
