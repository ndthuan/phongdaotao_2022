<?php

namespace App\Http\Requests\Validations;

use Illuminate\Http\Request;
use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Request::segment(count(Request::segments()));
        
        $rules = [];
        $rules['name'] = 'bail|required|unique:roles,name,'.$id;

        if (Request::user()->accessLevel()) {
            $rules['level'] = 'nullable|integer|between:'.Request::user()->accessLevel().','.(99);
        }

        if (Request::input('level') && !Request::user()->accessLevel()) {
            Request::replace(['level' => Null]); //Reset the level
        }

        return $rules;
    }
}
