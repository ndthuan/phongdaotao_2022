<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'level'         => $this->level,
            'users_count'   => $this->users->count(),
            'description'   => $this->description,
            'permissions'   => collect($this->permissions)->map(function ($permission) { 
                return [
                    'id'        => $permission->id,
                    'module_id' => $permission->module_id,
                    'name'      => $permission->name,
                    'slug'      => $permission->slug,
                ];
            }),
            'created_at'    => $this->created_at,
            'updated_at'    => $this->updated_at,
        ];
    }
}