<?php
namespace App\Traits;

use App\Helpers\Authorize;
use Illuminate\Support\Str;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

trait Authorizable
{
        /**
     * Group abilities
     * @var array
     */
    private $_abilities = [
        'index'     => 'view',
        'view'      => 'view',
        'all'       => 'view',
        'show'      => 'view',
        'list-of-user' => 'view',
        'user-by-project' => 'view',
        'project-follow-position-user' => 'view',
        'get-by-project' => 'view',

        'create'    => 'add',
        'store'     => 'add',

        'update'    => 'edit',
        'add-user'  => 'edit',
        'update-status-join-project' => 'edit',
        'cancel-join-project'        => 'edit',

        'delete'    => 'delete',
        'destroy'   => 'delete',
    ];

    /**
     * Override of callAction to perform the authorization before
     *
     * @param $method
     * @param $parameters
     * @return mixed
     */
    public function callAction($method, $parameters)
    {
        if (! $this->checkPermission('', $parameters)) {
            return response()->json([
                'success' => false,
                'message' => 'Không có quyền truy cập'
            ], Response::HTTP_FORBIDDEN);
        }

        return parent::callAction($method, $parameters);
    }
    
    /**
     * CheckPermission for this action with the given slug.
     * If the logged in user is the Super Admin Or
     * the given slug is 'Dashboard' then no need to check the permission
     *
     * @param string $slug
     * @param [type] $model
     *
     * @return bool : false if the permission not granted
     */
    public function checkPermission($slug, $params = null)
    {
        $slug = $slug ? $slug : $this->getSlug();

        return (new Authorize(Auth::user(), $slug, $params))->check();
    }
    
    /**
     * Get the slug to check the action permission
     *
     * @param $action
     * @param $module
     * @return string $slug
     */
    public function getSlug($action = null, $module = null)
    {
        $nameOfRoutes = explode('.', Request::route()->getName());

        $module = $module ? $module : array_splice($nameOfRoutes, -2, 1)[0];
        $action = $action ? $action : array_splice($nameOfRoutes, -1, 1)[0];

        return array_key_exists($action, $this->_abilities)
                ? $this->_abilities[$action].'_'.Str::snake($module)
                : $action;
    }
}