<?php

namespace App\Repositories;

use Illuminate\Http\Request;

interface BaseRepository
{
    public function all($fields = ['*']);

    public function find($id);

    public function findBy($field, $value);

    public function recent($limit);

    public function store(Request $request);

    public function update(Request $request, $id);

    public function destroy($id);

}