<?php

namespace App\Repositories;

use Illuminate\Http\Request;

abstract class EloquentRepository implements BaseRepository
{
    protected $model;
    public function __construct()
    {
        $this->model = app()->make($this->model());
    }
    
    abstract public function model();

    public function all($fields = ['*'])
    {
        return $this->model->get($fields);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findBy($field, $value)
    {
        return $this->model->where($field, $value);
    }

    public function recent($limit)
    {
        return $this->model->take($limit)->get();
    }

    public function store(Request $request)
    {
        $model = $this->model->create($request->all());

        if ($request->hasFile('images')) {
            foreach ($request->images as $type => $file) {
                $dir = config('image.dir.' . $this->model->getTable()) ?: config('image.dir.default');
                if (is_numeric($type)) {
                    $model->saveImage($file, $dir);
                } else {
                    $model->saveImage($file, $dir, $type);
                }
            }
        }
        return $model;
    }

    public function update(Request $request, $id)
    {
        $model = $this->model->find($id);
        $model->update($request->all());
	    if ($request->input('delete_images')){
            foreach ($request->delete_images as $type => $value) {
                $model->deleteImageTypeOf($type);
            }
        }
        if ($request->hasFile('images')) {
            $dir = config('image.dir.' . $this->model->getTable()) ?: config('image.dir.default');
            foreach ($request->images as $type => $file) {
                $model->updateImage($file, $dir, $type);
            }
        }
        return $this->model->find($id);
    }

    public function destroy($id)
    {
        return $this->model->find($id)->forceDelete();
    }
}