<?php

namespace App\Repositories\Blog;

use Illuminate\Http\Request;

interface BlogRepositoryInterface
{
    public function list(Request $request);
}