<?php

namespace App\Repositories\Blog;


use App\Models\Blog;
use Illuminate\Http\Request;
use App\Services\QueryService;
use App\Repositories\EloquentRepository;
use App\Repositories\Blog\BlogRepositoryInterface;

class BlogRepository extends EloquentRepository implements BlogRepositoryInterface
{
    public function model()
    {
        return \App\Models\Blog::class;
    }


    public function list(Request $request)
    {
        $search = $request->get('search', null);
        $limit = $request->get('limit', config('constants.pagination_limit'));

        $builder = new QueryService($this->model);

        $builder->search = $search;

        $builder->columnSearch = [
            'title',
        ];

        $builder = $builder->queryTable();
        return $builder->paginate($limit);
    }

}