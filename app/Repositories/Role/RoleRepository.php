<?php

namespace App\Repositories\Role;


use Illuminate\Http\Request;
use App\Services\QueryService;
use App\Repositories\EloquentRepository;
use App\Models\Role;

class RoleRepository extends EloquentRepository implements RoleRepositoryInterface
{
    public function model()
    {
        return \App\Models\Role::class;
    }

    public function list(Request $request)
    {
        $search = $request->get('search', null);
        $limit = $request->get('limit', config('constants.pagination_limit'));
        $builder = new QueryService($this->model);
        $builder->scope = ['lowerPrivileged'];
        $builder->search = $search;
        $builder->columnSearch = [
            'name',
            'level',
        ];
        $builder = $builder->queryTable();
        return $builder->paginate($limit);
    }

    public function store(Request $request)
    {
        $role = parent::store($request);

        $this->syncPermissions($role, $request->input('permissions', []));

        return $role;
    }

    public function update(Request $request, $id)
    {
        $role = parent::update($request, $id);
        
        $this->syncPermissions($role, $request->input('permissions', []));

        return $role;
    }

    public function syncPermissions($role, $permissions)
    {
        $role->permissions()->sync($permissions);
    }
}