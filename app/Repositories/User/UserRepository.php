<?php

namespace App\Repositories\User;

use Illuminate\Http\Request;
use App\Services\QueryService;
use App\Repositories\EloquentRepository;
use App\Repositories\User\UserRepositoryInterface;

class UserRepository extends EloquentRepository implements UserRepositoryInterface
{
    public function model()
    {
        return \App\Models\User::class;
    }

    public function list(Request $request)
    {
        $search = $request->get('search', null);
        $limit = $request->get('limit', config('constants.pagination_limit'));

        $builder = $this->model;
        $builder = $builder->leftJoin('roles', 'roles.id', '=', 'users.role_id');
        $builder = new QueryService($builder);
        $builder->scope = ['level'];
        $builder->search = $search;
        $builder->columnSearch = [
            'users.name',
            'users.email',
            'roles.name',
        ];
        $builder->select = ['users.*'];
        $builder->orderBy = 'users.created_at';
        $builder = $builder->queryTable();
        return $builder->paginate($limit);
    }
}