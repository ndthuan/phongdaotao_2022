<?php

if (! function_exists('get_storage_image_url'))
{
    function get_storage_image_url($path, $size = 'default')
    {
        if (empty($path)) {
            return get_placeholder_image($size);
        }

        if (empty($size)) {
            return url($path);
        }

        $size  = config('image.sizes.' . $size);

        return url("{$path}?width={$size['w']}&height={$size['h']}&fit={$size['fit']}");
    }
}

if (! function_exists('get_placeholder_image'))
{
    function get_placeholder_image($size)
    {
        $size = config("image.sizes.{$size}");

        if ($size && is_array($size)) {
            return "https://placehold.it/{$size['w']}x{$size['h']}/eee?fit={$size['fit']}&text=" . trans('app.no_img_available');
        }

        return url("images/placeholders/no_img.png");
    }
}
