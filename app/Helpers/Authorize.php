<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class Authorize
{
    protected $user;
    protected $params;
    protected $slug;

    private $_action_exceptional = [
        'public', 'welcome',
    ];

    public function __construct($user, $params, $slug){
        $this->user = $user;
        $this->params = $params;
        $this->slug = $slug;
    }

    public function check() 
    {
        if ($this->_isExceptional()) {
            return true;
        }

        return in_array($this->slug, $this->_permissionSlug());
    }

    private function _isExceptional()
    {
        if (in_array($this->slug, $this->_action_exceptional)) {
            return true;
        }

        if (Auth::user()->isSuperAdmin) {
            return true;
        }

        return false;
    }

    private function _permissionSlug()
    {
        return $this->user->role->permission()->pluck('slug')->toArray();
    }
}