<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    use HasFactory;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modules';

    /**
     * The attribute that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'access',
        'actions',
        'active',
    ];

    public function permissions()
    {
        return $this->hasMany(Permission::class);
    }
}
