<?php

namespace App\Models;

use App\Traits\Imageable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Blog extends Model
{
    use Imageable;

    protected $tabel = 'blogs';

    protected $fillable = [
        'title',
        'slug',
        'content',
        'category_id'
    ];
}
