import Vue from 'vue';
import App from './views/App.vue';
import router from '@/http/router';
import store from '@/store';
import ElementUI from 'element-ui';
import locale_en from 'element-ui/lib/locale/lang/en';

import '@/plugin/vue-plugin';
import '@/components/Global';
import '@/styles/element-variables.scss';

Vue.use(ElementUI, {
    size: 'medium',
    locale: locale_en,
});

// disable show warning async validator
// eslint-disable-next-line no-console
const warn = console.warn;
// eslint-disable-next-line no-console
console.warn = (...args) => {
    if (typeof args[0] === 'string' && args[0].startsWith('async-validator:')) {
        return;
    }
    warn(...args);
};

new Vue({
    store,
    router,
    ...App,
});
