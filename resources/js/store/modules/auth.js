import { getToken, setToken, removeToken } from '@/utils/auth';

export const state = {
    token: getToken() || null,
    role: {
        id: 1,
        name: 'Super Admin',
    },
    permissions: [],
    user: null,
};

export const getters = {
    role: state => state.role,
    permissions: state => state.permissions,
    user: state => state.user,
    token: state => state.token,
};

export const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token;
        setToken(token);
    },
    RESET_AUTH: state => {
        state.token = null;
        state.user = null;
        state.role = null;
        state.permissions = [];
        removeToken();
    },
    SET_USER: (state, payload) => {
        state.user = payload.user;
        state.role = payload.role;
        state.permissions = payload.permissions;
    },
};

export const actions = {
    setToken({ commit }, { access_token }) {
        commit('SET_TOKEN', access_token);
    },
    resetAuth({ commit }) {
        commit('RESET_AUTH');
    },
    setUser({ commit }, payload) {
        commit('SET_USER', payload);
    },
};
