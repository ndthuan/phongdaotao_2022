export function view(path) {
    return () => import(`@/views/${path}`).then(m => m.default || m);
}

export function fConfig(path) {
    if (!path.length) {
      return undefined;
    }

    const requireContextConfig = require.context('@/config', false, /\.js$/);
    const configs = requireContextConfig.keys()
        .map(file =>
            [file.replace(/(^.\/)|(\.js$)/g, ''), requireContextConfig(file)]
            )
        .reduce((modules, [name, module]) => {
            if (module.namespaced === undefined) {
                module.namespaced = true;
            }
            return { ...modules, [name]: module };
    }, {});

    function getDeepData(array, dataGet) {
        if (array.length === 0) {
            return dataGet;
        } else {
            dataGet = dataGet[array.shift()];
            return getDeepData(array, dataGet);
        }
    }
    const pathSplit = path.split('.');
    return getDeepData(pathSplit, configs[pathSplit.shift()]);
}

/**
 * @param {string} string
 * @param {string} expressions
 * @returns {boolean}
 */
export function matchInArray(string, expressions) {
    const len = expressions.length;
    let i = 0;

    for (; i < len; i++) {
        if (string.match(expressions[i])) {
            return true;
        }
    }

    return false;
}
