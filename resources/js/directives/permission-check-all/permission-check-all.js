export default {
    inserted(el, binding, vnode) {
      const ATTR_MODULE = 'data-module-id';
      const ATTR_PERMISSION = 'data-permission-id';
      const ATTR_BELONGS_TO_MODULE = 'data-belongs-module-id';

      const $tableDIV = el;
      const $inputCheckAllList = $tableDIV.querySelectorAll(`[${ATTR_MODULE}]`);
      const $inputCheckItemList = $tableDIV.querySelectorAll(`[${ATTR_BELONGS_TO_MODULE}]`);
      const { role, permissionIds, superAdminId } = Object.assign({}, binding.value);

      (function () {
        if (role.id === superAdminId) {
          assignIsCheck($inputCheckAllList, true);
          assignIsCheck($inputCheckItemList, true);
        } else {
          if ($inputCheckItemList && permissionIds.length) {
            const $permission = [...$inputCheckItemList].filter($item => permissionIds.includes(+$item.getAttribute(`${ATTR_PERMISSION}`)));
            assignIsCheck($permission, true)
              .groupByAttr(`${ATTR_BELONGS_TO_MODULE}`, function (data) {
                if (typeof data === 'object') {
                  for (const [moduleId, $permissions] of Object.entries(data)) {
                    const $permissionBelongsToModule = $tableDIV.querySelectorAll(`[${ATTR_BELONGS_TO_MODULE}="${moduleId}"]`);
                    if ($permissionBelongsToModule.length === $permissions.length) {
                      assignIsCheck([$tableDIV.querySelector(`[${ATTR_MODULE}="${moduleId}"]`)], true);
                    }
                  }
                }
              });
          }
        }
      })();

      // Handle check input group
      $inputCheckAllList.forEach($item => {
        $item.onclick = function () {
          const moduleId = this.getAttribute(`${ATTR_MODULE}`);
          const $permissionBelongsToModule = $tableDIV.querySelectorAll(`[${ATTR_BELONGS_TO_MODULE}="${moduleId}"]`);
          if (this.checked) {
            assignIsCheck($permissionBelongsToModule, true);
          } else {
            assignIsCheck($permissionBelongsToModule, false);
          }
        };
      });

      // Handle check input item
      $inputCheckItemList.forEach($item => {
        $item.onclick = function () {
          const moduleId = $item.getAttribute(`${ATTR_BELONGS_TO_MODULE}`);
          const $module = $tableDIV.querySelector(`[${ATTR_MODULE}="${moduleId}"]`);
          const $belongsToModule = $tableDIV.querySelectorAll(`[${ATTR_BELONGS_TO_MODULE}="${moduleId}"]`);
          const totalChecked = [...$belongsToModule].filter($el => $el.checked).length;
          if (+totalChecked === [...$belongsToModule].length) {
            assignIsCheck([$module], true);
          } else {
            assignIsCheck([$module], false);
          }
        };
      });

      /**
       * Assign check for el input
       */
      function assignIsCheck(els, isCheck) {
        els.forEach(el => {
          el.checked = isCheck;
        });

        // Closure
        function groupByAttr(by, callback) {
          const result = {};
          els.forEach(el => {
            const dataBy = el.getAttribute(by);
            if (result[dataBy]) {
              result[dataBy].push(el);
            } else {
              result[dataBy] = [el];
            }
          });
          return callback(result);
        }

        return {
          groupByAttr,
        };
      }
    },
};
