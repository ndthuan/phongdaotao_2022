import permissionCheckAll from './permission-check-all';

const install = function (Vue) {
  Vue.directive('permission-check-all', permissionCheckAll);
};

if (window.Vue) {
  window.permissionCheckAll = permissionCheckAll;
  Vue.use(install) // eslint-disable-line
}

permissionCheckAll.install = install;
export default permissionCheckAll;
