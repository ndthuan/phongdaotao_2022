import store from '@/store';
import { SUPER_ADMIN_ROLE_ID } from '@/config/constants';
import permission from '.';

export default {
    inserted (el, binding, vnode) {
        const { value } = binding;

        const stateStore = {
            role: store.getters['auth/role'],
            permissions: store.getters['auth/permissions']
        }

        //check supper admin role
        if (stateStore.role && stateStore.role.id === SUPER_ADMIN_ROLE_ID) {
            return true;
        }

        //analy value to decide return
        if (value && value instanceof Array && value.length > 0) {
            const permissionRequired = value;

            if (checkPermissionHasRequired(permissionRequired)) {
                const is = permissionRequired.every(permission => {
                    const permissionArr = permission.split(':');
                    if (permissionArr[1] === 'required') {
                        return stateStore.permissions.map(item => item.slug).includes(permissionArr[0]);
                    } else {
                        return true;
                    }
                })
            } 

            // check xem user có permission hay không (check trong store), và xem thử ds permission của user có 
            const hasEnoughPermissions = stateStore.permissions && stateStore.permissions.some(permission => {
                return permissionRequired.includes(permission.slug);
            })

            if (!hasEnoughPermissions) {
                removeDom();
            }
        } else {
            throw new Error("Permissions are required! Example: v-permission=['view_user', 'add_user', ...]");
        }

        function checkPermissionHasRequired (permission) {
            //lọc qua từng permission trong khai báo directive, tách dấu : sau đó đưa vào hàm some mà vẫn có giá trị [1] === 'required'
            return permission.some(permission => {
                return permission.split(':')[1] === 'required';
            });
        }

        function removeDom(){
            el.parentNode && el.parentNode.removeChild(el);
        }
    }
}