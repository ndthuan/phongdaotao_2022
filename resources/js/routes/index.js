import { view } from '@/utils/helpers';

export default [
    // Auth Routes
    {
        path: '/login',
        name: 'auth.login',
        component: view('auth/Login'),
    },
    {
        path: '/reset-password',
        name: 'auth.reset-password',
        component: view('auth/VerifyEmailForm'),
    },
    {
        path: '/reset-password/:token',
        name: 'auth.reset-password.token',
        component: view('auth/ResetPasswordForm'),
    },
    {
        path: '/admin/dashboard',
        name: 'admin.dashboard.index',
        component: view('dashboard/index'),
    },
    // Role router
    {
        path: '/admin/role',
        name: 'admin.role.index',
        component: view('admin/role/index'),
    },
    // User router
    {
        path: '/admin/user',
        name: 'admin.user.index',
        component: view('admin/user/index'),
    },
    {
        path: '*',
        name: 'error404',
    },
];
