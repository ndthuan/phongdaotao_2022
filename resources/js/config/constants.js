/** @desc App configuration */
export const CONST_APP = {
    base_api: process.env.MIX_BASE_API,
    token_key: 'blog-auth-token',
};

/** @desc Auth configuration */
export const CONST_AUTH = {
    global_middleware: ['auth-global'],
    default_expire: 1,
};

/** @desc Axios configuration */
export const CONST_AXIOS = {
    timeout: 60000,
};

/** @desc Axios configuration */
export const SUPER_ADMIN_ROLE_ID = 1;
