module.exports = [
    {
        name: 'Dashboard',
        icon: 'el-icon-s-home',
        route: 'admin.dashboard.index',
        access: 'admin',
    },
    {
        name: 'Members',
        icon: 'el-icon-user',
        access: 'admin',
        children: [
            {
                name: 'user',
                icon: '',
                route: 'admin.user.index',
                access: 'admin',
            },
            {
                name: 'role',
                icon: '',
                route: 'admin.role.index',
                access: 'admin',
            },
        ],
    },
    {
        name: 'Catalog',
        icon: 'el-icon-document-copy',
        access: 'admin',
        children: [
            {
                name: 'Categories',
                icon: '',
                route: 'admin.dashboard.index',
                access: 'admin',
            },
            {
                name: 'Blog',
                icon: '',
                route: 'admin.dashboard.index',
                access: 'admin',
            },
        ],
    },
];
