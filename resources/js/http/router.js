import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from '@/routes';
import { CONST_AUTH } from '@/config/constants';

Vue.use(VueRouter);

export const creatRouter = () => {
    const router = new VueRouter({
        linkActiveClass: 'active',
        mode: 'history',
        routes,
        scrollBehavior: to => {
            if (to.hash) {
                return { selector: to.hash };
            } else {
                return { x: 0, y: 0 };
            }
        },
    });
    router.beforeEach(beforeEach);
    router.afterEach(afterEach);

    return router;
};

const router = creatRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = creatRouter();
    router.matcher = newRouter.matcher; // reset router
}

export default router;

function resolveMiddleware(requireContext) {
    return requireContext.keys()
        .map(file =>
            [file.replace(/(^.\/)|(\.js$)/g, ''), requireContext(file)]
        )
        .reduce((guards, [name, guard]) => (
            { ...guards, [name]: guard.default }
        ), {});
}

const routeMiddleware = resolveMiddleware(
    require.context('@/http/middleware', false, /.*\.js$/)
);

function resolveComponents(components) {
    return Promise.all(components.map(component => {
        return typeof component === 'function' ? component() : component;
    }).filter(component => !!component));
}

function getMiddleware(components) {
    const middleware = [...CONST_AUTH.global_middleware];
    components.filter(c => c.middleware).forEach(component => {
        if (Array.isArray(component.middleware)) {
            middleware.push(...component.middleware);
        } else {
            middleware.push(component.middleware);
        }
    });
    return middleware;
}

function callMiddleware(middleware, to, from, next) {
    const stack = middleware.reverse();
    const _next = (...args) => {
        if (args.length > 0 || stack.length === 0) {
            return next(...args);
        }
        const { middleware, params } = parseMiddleware(stack.pop());
        if (typeof middleware === 'function') {
            middleware(to, from, _next, params);
        } else if (routeMiddleware[middleware]) {
            routeMiddleware[middleware](to, from, _next, params);
        } else {
            throw Error(`Undefined middleware [${middleware}]`);
        }
    };
    _next();
}

function parseMiddleware(middleware){
    if (typeof middleware === 'function') {
        return { middleware };
    }

    const [name, params] = middleware.split(':');
    return { middleware: name, params };
}

// ===== ${{ HANDLE MIDDLEWARE }}$ ===== //
async function beforeEach(to, from, next) {
    let components = [];
    try {
        components = await resolveComponents(
            router.getMatchedComponents({ ...to })
        );
    } catch (error) {
        if (/^Loading( CSS)? chunk (\d)+ failed\./.test(error.message)) {
            window.location.reload(true);
            return;
        }
    }
    if (components.length === 0) {
        return next();
    }

    // Get all middleware of the matched components
    const middleware = getMiddleware(components);

    // Call each middleware
    callMiddleware(middleware, to, from, (...args) => {
        // Set the application layout only if "next()" was called with no args.
        if (args.length === 0) {
            router.app.setLayout(components[0].layout || '');
        }
        next(...args);
    });
}

async function afterEach(to, from, next) {

}
