import Resource from '@/http/api/resource';
import request from '@/http/request';

export default class UserResource extends Resource {
    constructor() {
        super('/admin/user');
    }

    store(resource) {
        return request({
            url: this.uri,
            method: 'post',
            data: resource,
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
    }

    update(resource, id) {
        return request({
            url: `${this.uri}/${id}`,
            method: 'post',
            params: {
                _method: 'put',
            },
            data: resource,
            headers: {
                'Content-Type': 'multipart/form-data',
            },
        });
    }
}
