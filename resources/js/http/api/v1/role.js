import request from '@/http/request';
import Resource from '@/http/api/resource';

export default class RoleResource extends Resource {
    constructor() {
        super('role');
    }
    getAll() {
        return request({
            url: `${this.uri}/get-all`,
            method: 'GET',
        });
    }
}
