import store from '@/store';
import AuthResource from '@/http/api/v1/auth';

export default async (to, from, next) => {
    const authResource = new AuthResource();
    const user = store.getters['auth/user'];
    const token = store.getters['auth/token'];

    if (!user && !!token) {
        try {
            const userAuth = await authResource.getUserAuth();
            store.dispatch(`auth/setUser`, userAuth.data.data);
        } catch (e) {
            //
        }
    }
    next();
};
