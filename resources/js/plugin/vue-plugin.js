import Vue from 'vue';
import store from '@/store';
import { SUPER_ADMIN_ROLE_ID } from '@/config/constants';

const corePlugin = {
  install(Vue, options) {
    Vue.prototype.$authorize = (permissionRequired, callback = null) => {
      const stateStore = {
        role: store.getters['auth/role'],
        permissions: store.getters['auth/permissions'],
      };
      if (stateStore.role && stateStore.role.id === SUPER_ADMIN_ROLE_ID) {
        if (typeof callback === 'function') {
          return callback();
        }
        return Promise.resolve({ data: { authorize: true }});
      }

      if (checkPermissionHasRequired(permissionRequired)) {
        const is = permissionRequired.every(permission => {
          const permissionArr = permission.split(':');
          if (permissionArr[1] === 'required') {
            return stateStore.permissions.map(item => item.slug).includes(permissionArr[0]);
          } else {
            return jsonResponse(true, { authorize: true });
          }
        });

        if (!is) {
          return jsonResponse(false, { authorize: false });
        }
      }

      const hasEnoughPermissions = stateStore.permissions.some(permission => {
        return permissionRequired.includes(permission.slug);
      });

      if (hasEnoughPermissions) {
        return jsonResponse(true, { authorize: true });
      }

      return jsonResponse(false, { authorize: false });

      // Functions
      function checkPermissionHasRequired(permissions) {
        return permissions.some(permission => {
          return permission.split(':')[1] === 'required';
        });
      }

      function jsonResponse(isCheck, data = {}) {
        if (isCheck) {
          if (typeof callback === 'function') {
            return callback(data);
          }
          return Promise.resolve({ data: data });
        }

        if (typeof callback === 'function') {
          return Promise.resolve(data);
        }
        return Promise.reject(data);
      }
    };
  },
};

Vue.use(corePlugin);
