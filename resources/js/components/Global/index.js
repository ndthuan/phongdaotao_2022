import Vue from 'vue';
import TargetLayout from './Target';

[
    TargetLayout,
].forEach(comp => {
    Vue.component(comp.name, comp);
});
